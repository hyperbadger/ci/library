# library

Library for standard predefined CI pipelines, template job snippets and container images for use in CI.

## Our take on CI pipeline 🛠️

There are 3 ways in which CI pipeline can be defined across the various project repositories.

1. Use a predefined set of CI pipeline jobs grouped for a specific purpose 📋
1. Use a set of template job snippets defined for a specific purpose 📑
1. Use your own CI definition 📝

Prdefined set of CI pipeline and template job snippets are defined as part of this `library` repository and can be used in any other repository's CI file. 

We recommend using the top to bottom approach on the above defined ways and write your own CI definition only when something is not available in `library`. If it is going to be a repetitive CI definition, please consider adding it as part of `library`.

In order to use the approach 1 or 2 in your CI definition, use the below `include` definition in your `.gitlab-ci.yml` file.

```
include:
  - project: hyperbadger/ci/library
    ref: <required_branch_or_tag>
    file: <predefined_ci_yaml_file>
```

or 

```
include:
  - project: hyperbadger/ci/library
    ref: <required_branch_or_tag>
    file: snippets/<predefined_template_file>
```

## Images

Below are the list of images managed by this repository:

- **infratools**: Used to share common platform tools dependencies; primarily used in all the terraform CI to manage infrastucture deployments
- **check**: Used to perform all code check actions such as yamllint, shellcheck, pylint, black etc..
- **nfpm**: Used to build deb, rpm or apk packages and push to generic package registry
- **pypi**: Used to perform python packing and push to generic package registry (future scope: packaging and pushing to public repo)

## Which image to use

To use the stable tagged version of an image:

1. For the image tag, go to [tags](https://gitlab.com/hyperbadger/ci/library/-/tags) and note down latest tag of the required image in the SemVer format 
    > **_NOTE:_** Remove the "`-<image_name>`" suffix from the tag and use just the SemVer format
1. Go to [container-registry](https://gitlab.com/hyperbadger/ci/library/container_registry), select the required image and copy the requried tag image path (use the copy button 📋)

## Contributing

Refer to [CONTRIBUTING.md](https://gitlab.com/hyperbadger/ci/library/-/blob/main/CONTRIBUTING.md) on how to release changes to this repository, create new image or manage existing images
