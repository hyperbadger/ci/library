# Contributing

## Adding new template or predefined CI

1. Create a new file under `snippets` directory and give a name that defines the purpose; for eg. `docker.yml` for docker image build and publish
1. Write your template CI job definitions in the yml file (you can also reference CI definition from another snippets yml file using include)
1. Create a yml file in the root directory of `library` if you want a predefined CI for which the template job snippet was created
1. Create a tag once changes are merged to main branch, tag your repo with a SemVar that makes sense
1. Use the newly created tag ref in the `include` definition of target repository's `.gitlab-ci.yml` file

## Updating existing CI template

1. Make your changes to the required yml files - once you push these changes, use branch ref to validate the CI update
1. Once you are happy with your changes, create an MR and get things merged
1. Before merging, update the new tag ref (which is yet to be created) in the `include` definition of internal CI files where the changes are required
1. After merging, tag your repo with a SemVar that makes sense
1. Update the newly created tag ref in the `include` definition of all repo's `.gitlab-ci.yml` file where the changes are required

## Creating a new image

1. Create a new folder with a name matching your intended image name under the `images` directory
1. Write your Dockerfile
1. Modify the `.gitlab-ci.yml` file to add a `build-` and `release-` sections for your new image - use the existing build and release jobs as examples to help you

## Modifying an existing image

1. Make your changes to the Dockerfile - once you push these changes, an image should be build for your branch which you can test
1. Once you are happy with your changes, create an MR and get things merged
1. After merging, tag your repo with a SemVar that makes sense - look at the existing tags for that image and choose the next logical bump; once your version number is decided, tag with the image name appended to the end like so `1.2.3-image-name` - this image name should match the folder of the Dockerfile you modified
